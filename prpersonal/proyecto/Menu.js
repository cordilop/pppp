import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {  StyleSheet, Button, Text} from 'react-native';
import Nosotros from './Nosotros';
import { Ionicons } from '@expo/vector-icons';
import CitasScreen from './CitasScreen';
import 'react-native-gesture-handler';
import { Drawer } from 'react-native-drawer-layout';
import MaquinaScreen from './MaquinaScreen';
import 'react-native-gesture-handler';
import PerfilScreen from './PerfilScreen'; 



const Tab = createBottomTabNavigator();
const Logoo = require("./assets/Images/logo.png");


export function MenuTabs() {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;

          if (route.name === 'Sobre Nosotros') {
            iconName = focused ? 'information-circle' : 'information-circle-outline';
        } else if (route.name === 'Citas') {
            iconName = focused ? 'calendar' : 'calendar-outline';
        } else if (route.name === 'Maquinas') {
            iconName = focused ? 'settings' : 'settings-outline';
        } else if (route.name === 'Perfil') {
            iconName = focused ? 'person' : 'person-outline'; // Utilizando un icono de perfil
        } else {
            // Asignar un icono por defecto para otras rutas
            iconName = focused ? 'home' : 'home-outline'; // Por ejemplo, un icono de casa
        }
        


          return <Ionicons name={iconName} size={size} color={color} />;
        },
        tabBarActiveTintColor: '#04b9f0',
        tabBarInactiveTintColor: 'gray',
        tabBarStyle: {
          display: 'flex',
        },
      })}
    >
      <Tab.Screen name="Sobre Nosotros" component={Nosotros} />
      <Tab.Screen name="Citas" component={CitasScreen} />
      <Tab.Screen name="Maquinas" component={MaquinaScreen} />
      <Tab.Screen name="Perfil" component={PerfilScreen} /> 
    </Tab.Navigator>



  );
}



export function SignupForm() {
  return(
    <Tab.Navigator>
      <Tab.Screen name="Crear cuenta" component={SignupScreen} />
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  containerCita: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    
  },
  scrollContainer: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  TextInput: {
    padding: 10,
    paddingStart: 30,
    width: '60%',
    height: 35,
    marginTop: 20,
    borderRadius: 10,
    backgroundColor: '#f1f1f1',
    borderColor: '#000', 
    borderWidth: 0.3, 
  },
  TextSignup: {
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
  },
  menuButton: {
    marginTop: 20,
    borderRadius: 10,
  },
  linearGradient: {
    marginTop: 20,
    width: 170
    ,
    height: 50,
    borderRadius: 25,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuButtonText: {
    fontSize: 20,
    color: 'white',
    textAlign: 'center',
  },
  cancelar: {
    fontWeight: 'bold',
    paddingTop: 1,
    paddingBottom: 42,
  },
  title: {
    fontSize: 34,
    fontWeight: 'bold',
    marginBottom: 60,
    marginTop: 0,
  },
  subtitle: {
    fontSize: 23,
    marginTop: 50,
    marginBottom: 40,
  },
  sidebar: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 40,
    paddingHorizontal: 20,
    borderRightColor: '#e5e5e5',
    borderRightWidth: 1,
  },
  sidebarItem: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  drawerContent: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 40,
    paddingHorizontal: 20,
    borderRightColor: '#e5e5e5',
    borderRightWidth: 1,
  },
  drawerItem: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
});