import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const MaquinaScreen = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Menú de Cocina</Text>
      <Text style={styles.menuItem}>- Pizza</Text>
      <Text style={styles.menuItem}>- Hamburguesa</Text>
      <Text style={styles.menuItem}>- Ensalada</Text>
      <Text style={styles.menuItem}>- Pasta</Text>
      <Text style={styles.menuItem}>- Sopa</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  menuItem: {
    fontSize: 18,
    marginBottom: 10,
  },
});

export default MaquinaScreen;
