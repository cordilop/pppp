import React from "react";
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

    export default function ButtonsGradients () {
        return(
            <TouchableOpacity style={styles.container}>
                <LinearGradient
                    colors={['#04b9f0', '#50d1f8', '#50d1f8']} //Colores hexadecimales de los botones
                    start={{x: 1, y: 0}}
                    end={{x: 0, y:1}}
                    style={styles.button}
                >
                    <Text style={styles.text}>Iniciar Sesión</Text>
                </LinearGradient>
            </TouchableOpacity>
        )
    }

const styles = StyleSheet.create({
    container: {
        width: 200,
        marginTop: 60,
    },
    text: {
        fontSize: 14,
        color: '#fff',
        fontWeight: 'bold',
    },
    button: {
        width: '#80%',
        height: 50,
        borderRadius: 25,
        padding: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
});