import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';

export function Sidebar() {
  const navigation = useNavigation();

  return (
    <View style={styles.sidebar}>
      <TouchableOpacity onPress={() => navigation.navigate('Sobre Nosotros')}>
        <Text style={styles.sidebarItem}>Sobre Nosotros</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.navigate('Citas')}>
        <Text style={styles.sidebarItem}>Citas</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => navigation.navigate('Maquinas')}>
        <Text style={styles.sidebarItem}>Maquinas</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  sidebar: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 40,
    paddingHorizontal: 20,
    borderRightColor: '#e5e5e5',
    borderRightWidth: 1,
  },
  sidebarItem: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
});

export default Sidebar;
