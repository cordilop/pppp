import React from 'react';
import { View, Text, StyleSheet, TextInput, ScrollView, TouchableOpacity } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { useNavigation } from '@react-navigation/native';

export default function SignupScreen() {
  const navigation = useNavigation();
  return (
    <ScrollView contentContainerStyle={styles.scrollContainer} style={styles.container}>
      <Text style={styles.TextSignup}>Nombre Moral</Text>
      <TextInput 
        style={styles.TextInput}
      />
      <Text style={styles.TextSignup}>Número telefonico</Text>
      <TextInput 
        style={styles.TextInput}
      />
      <Text style={styles.TextSignup} >Nombre de pila</Text>
      <TextInput 
        style={styles.TextInput}
      />
      <Text style={styles.TextSignup} >Primer apellido</Text>
      <TextInput 
        style={styles.TextInput}
      />
      <Text style={styles.TextSignup} >Segundo apellido</Text>
      <TextInput 
        style={styles.TextInput}
      />
      <Text style={styles.TextSignup} >Colonia</Text>
      <TextInput 
        style={styles.TextInput}
      />
      <Text style={styles.TextSignup} >Calle</Text>
      <TextInput 
        style={styles.TextInput}
      />
      <Text style={styles.TextSignup} >Código Postal</Text>
      <TextInput 
        style={styles.TextInput}
      />
      <Text style={styles.TextSignup} >Nombre de usuario</Text>
      <TextInput 
        style={styles.TextInput}
      />
      <Text style={styles.TextSignup} >Contraseña</Text>
      <TextInput 
        style={styles.TextInput}
      />
      <Text style={styles.TextSignup} >Confirmar contraseña</Text>
      <TextInput 
        style={styles.TextInput}
      />

      <TouchableOpacity onPress={() => navigation.navigate('Menu')} style={styles.menuButton}>
        <LinearGradient
          colors={['#04b9f0', '#50d1f8', '#50d1f8']}
          start={{x: 1, y: 0}}
          end={{x: 0, y:1}}
          style={styles.linearGradient}>
          <Text style={styles.menuButtonText}>Crear Cuenta</Text>
        </LinearGradient>
      </TouchableOpacity>

      <TouchableOpacity onPress={() => navigation.navigate('Home')} style={styles.menuButton}>
        <LinearGradient
          colors={['#fff', '#fff', '#fff']}
          start={{x: 1, y: 0}}
          end={{x: 0, y:1}}
          style={styles.linearGradient}>
          <Text style={styles.cancelar} >Cancelar</Text>
        </LinearGradient>
      </TouchableOpacity>

    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  scrollContainer: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  TextInput: {
    padding: 10,
    paddingStart: 30,
    width: '60%',
    height: 35,
    marginTop: 20,
    borderRadius: 10,
    backgroundColor: '#f1f1f1',
    borderColor: '#000', 
    borderWidth: 0.3, 
  },
  TextSignup: {
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
  },
  menuButton: {
    marginTop: 20,
    borderRadius: 10,
  },
  linearGradient: {
    marginTop: 20,
    width: 170,
    height: 50,
    borderRadius: 25,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuButtonText: {
    fontSize: 20,
    color: 'white',
    textAlign: 'center',
  },
  cancelar: {
    fontWeight: 'bold',
    paddingTop: 1,
    paddingBottom: 42,
  },
});
