import React from 'react';
import { StatusBar, Image, TouchableOpacity } from 'react-native';
import { StyleSheet, Text, View,Button, TextInput } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { LinearGradient } from 'expo-linear-gradient';
import { MenuTabs} from './Menu'; 
import SignupScreen from './SignupScreen';
import 'react-native-gesture-handler';
import { Drawer } from 'react-native-drawer-layout';



const ImageTop = require("./assets/Images/top.jpg");

function HomeScreen({ navigation }) {
  return (
    <View style={styles.container}>
      <Image
        source={ImageTop}
        style={styles.imageTop}
      />
      <Text style={styles.title}>MachineryFix</Text>
      <Text style={styles.subtitle}>Iniciar sesión en su cuenta</Text>
      <TextInput 
        placeholder='Nombre de usuario'
        style={styles.TextInput}
      />
      <TextInput 
        placeholder='Contraseña'
        style={styles.TextInput}
        secureTextEntry={true}
      />
      <TouchableOpacity onPress={() => navigation.navigate('Menu')} style={styles.menuButton}>
        <LinearGradient
          colors={['#04b9f0', '#50d1f8', '#50d1f8']}
          start={{x: 1, y: 0}}
          end={{x: 0, y:1}}
          style={styles.linearGradient}>
          <Text style={styles.menuButtonText}>Iniciar sesión</Text>
        </LinearGradient>
      </TouchableOpacity>

      <TouchableOpacity onPress={() => navigation.navigate('Signup')}>
        <LinearGradient
          colors={['#fff', '#fff', '#fff']}
          start={{x: 1, y: 0}}
          end={{x: 0, y:1}}
          style={styles.linearGradient}>
          <Text style={styles.TextAccount}>No tienes una cuenta</Text>
        </LinearGradient>
      </TouchableOpacity>

      <StatusBar style="auto" />
    </View>
    
  );
}


const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen 
          name="Home"
          component={HomeScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen 
          name="Menu" 
          component={MenuTabs} 
          options={{ headerShown: false }}
        />
        <Stack.Screen 
          name="Signup" 
          component={SignupScreen}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}




const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageTop: {
    flex: 1,
    width: '100%',
    height: 270, 
    resizeMode: 'cover',
    position: 'absolute',
    top: 0,
  },
  title: {
    fontSize: 60,
    color: '#000',
    fontWeight: 'bold',
  },
  subtitle: {
    fontSize: 20,
    color: 'gray',
  },
  TextInput: {
    padding: 10,
    paddingStart: 30,
    width: '80%',
    height: 50,
    marginTop: 20,
    borderRadius: 30,
    backgroundColor: '#f1f1f1',
  },
  TextAccount: {
    fontSize: 14,
    color: 'gray',
    marginTop: 30,
    textAlign: 'center',
    alignItems: 'center',
    marginTop: 0,
  },
  menuButton: {
    marginTop: 20,
    borderRadius: 10,
  },
  menuButtonText: {
    fontSize: 20,
    color: 'white',
    textAlign: 'center',
  },
  linearGradient: {
    marginTop: 20,
    width: 160,
    height: 50,
    borderRadius: 25,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
