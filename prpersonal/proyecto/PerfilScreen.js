import React, { useState } from 'react';
import { View, Text, StyleSheet, Image, Button, TextInput, Picker } from 'react-native';
import * as ImagePicker from 'expo-image-picker';

const PerfilScreen = () => {
  const [fotoPerfil, setFotoPerfil] = useState(require('./assets/Images/perfil.jpg')); // Estado para la foto de perfil
  const [nombreCompleto, setNombreCompleto] = useState('Juan Pérez');
  const [nombreUsuario, setNombreUsuario] = useState('juanperez');
  const [estado, setEstado] = useState('Activo');
  const [biografia, setBiografia] = useState('¡Hola! Soy Juan, amante de la tecnología y la programación.');

  const [editing, setEditing] = useState(false); 

  const seleccionarFotoPerfil = async () => {
    const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
    if (status !== 'granted') {
      alert('Permiso denegado para acceder a la galería.');
      return;
    }
    
    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setFotoPerfil({ uri: result.uri });
    }
  };

  const guardarCambios = () => {
    setEditing(false); 
  };

  return (
    <View style={styles.container}>
      <Image source={fotoPerfil} style={styles.fotoPerfil} />
      {editing ? (
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.input}
            value={nombreCompleto}
            onChangeText={setNombreCompleto}
            placeholder="Nombre Completo"
          />
          <TextInput
            style={styles.input}
            value={nombreUsuario}
            onChangeText={setNombreUsuario}
            placeholder="Nombre de Usuario"
          />
          <Picker
            style={styles.input}
            selectedValue={estado}
            onValueChange={(itemValue, itemIndex) => setEstado(itemValue)}
          >
            <Picker.Item label="Activo" value="Activo" />
            <Picker.Item label="Inactivo" value="Inactivo" />
          </Picker>
          <TextInput
            style={[styles.input, { height: 100 }]}
            value={biografia}
            onChangeText={setBiografia}
            placeholder="Biografía"
            multiline
          />
          <Button title="Guardar Cambios" onPress={guardarCambios} />
        </View>
      ) : (
        <View>
          <Text style={styles.title}>{nombreCompleto}</Text>
          <Text style={styles.text}>Nombre de usuario: {nombreUsuario}</Text>
          <Text style={[styles.text, { color: estado === 'Activo' ? 'green' : 'red' }]}>Estado: {estado}</Text>
          <Text style={styles.subtitle}>Biografía:</Text>
          <Text style={styles.biografia}>{biografia}</Text>
          <Button title="Editar Perfil" onPress={() => setEditing(true)} />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    paddingHorizontal: 20,
  },
  fotoPerfil: {
    width: 150,
    height: 150,
    borderRadius: 75,
    marginBottom: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  text: {
    fontSize: 16,
    marginBottom: 5,
  },
  subtitle: {
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 10,
  },
  biografia: {
    fontSize: 16,
    marginBottom: 20,
    textAlign: 'center',
  },
  inputContainer: {
    width: '100%',
    alignItems: 'center',
  },
  input: {
    width: '80%',
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginBottom: 10,
  },
});

export default PerfilScreen;
