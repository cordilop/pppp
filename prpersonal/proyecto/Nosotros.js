import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Linking } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

const Nosotros = () => {
  const goToInstagram = () => {
    Linking.openURL('https://www.instagram.com/machineryfix.tij/');
  };

  const goToFacebook = () => {
    Linking.openURL('https://www.facebook.com/profile.php?id=61557538764623');
  };

  const goToTwitter = () => {
    Linking.openURL('https://twitter.com/nuestra_cuenta_twitter');
  };

  const openGoogleMaps = () => {
    Linking.openURL('https://maps.app.goo.gl/KExFzhfCkKu1gdgQ7');
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Síguenos en nuestras redes sociales:</Text>
      <TouchableOpacity onPress={goToInstagram} style={styles.socialContainer}>
        <Ionicons name="logo-instagram" size={30} color="#E1306C" style={styles.icon} />
        <Text style={styles.socialText}>Instagram</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={goToFacebook} style={styles.socialContainer}>
        <Ionicons name="logo-facebook" size={30} color="#1877F2" style={styles.icon} />
        <Text style={styles.socialText}>Facebook</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={goToTwitter} style={styles.socialContainer}>
        <Ionicons name="logo-twitter" size={30} color="#1DA1F2" style={styles.icon} />
        <Text style={styles.socialText}>Twitter</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={openGoogleMaps} style={styles.socialContainer}>
        <Ionicons name="locate" size={30} color="#34ebc5" style={styles.icon} />
        <Text style={styles.socialText}>Ubicación en Google Maps</Text>
      </TouchableOpacity>
      <View style={styles.imageContainer}>
        <Image source={require('./components/1.jpg')} style={styles.image} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    paddingHorizontal: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  socialContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20, 
  },
  icon: {
    marginRight: 10,
  },
  socialText: {
    fontSize: 18,
    color: '#333', 
    textDecorationLine: 'underline', 
  },
  imageContainer: {
    marginTop: 20,
  },
  image: {
    width: 200,
    height: 200,
    resizeMode: 'cover',
    borderRadius: 10,
  },
});

export default Nosotros;
