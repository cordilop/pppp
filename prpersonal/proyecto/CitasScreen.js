import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet,Image,Linking  } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { LinearGradient } from 'expo-linear-gradient';
import DatePicker from '@react-native-community/datetimepicker';



export default function CitasScreen() {
  const navigation = useNavigation();
  const [date, setDate] = useState(new Date());

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setDate(currentDate);
  };

  return (
    <View style={styles.containerCita}>
      <Text style={styles.title}>Agendar Cita</Text>
      <View style={styles.imageContainer}>
        <Image source={require('./assets/Images/logo.png')} style={styles.image} />
      </View>
      <Text style={styles.subtitle}>Fecha de la cita</Text>
      <DatePicker
        value={date}
        mode="date"
        is24Hour={true}
        display="default"
        onChange={onChange}
      />
      <Text style={styles.subtitle}>Hora de la cita</Text>
      <DatePicker
        value={date}
        mode="time"
        is24Hour={true}
        display="default"
        onChange={onChange}
      />

      <TouchableOpacity onPress={() => navigation.navigate('Menu')} style={styles.menuButton}>
        <LinearGradient
          colors={['#04b9f0', '#50d1f8', '#50d1f8']}
          start={{ x: 1, y: 0 }}
          end={{ x: 0, y: 1 }}
          style={styles.linearGradient}>
          <Text style={styles.menuButtonText}>Hacer Cita!</Text>
        </LinearGradient>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  containerCita: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  title: {
    fontSize: 34,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  imageContainer: {
    marginTop: 20,
  },
  subtitle: {
    fontSize: 23,
    marginTop: 20,
    marginBottom: 10,
  },
  menuButton: {
    marginTop: 20,
    borderRadius: 10,
  },
  linearGradient: {
    marginTop: 20,
    width: 170,
    height: 50,
    borderRadius: 25,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuButtonText: {
    fontSize: 20,
    color: 'white',
    textAlign: 'center',
  },
});
