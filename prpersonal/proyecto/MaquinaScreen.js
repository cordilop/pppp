import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

function MaquinaScreen() {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Maquinas Screen</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 34,
    fontWeight: 'bold',
    marginBottom: 60,
    marginTop: 0,
  },
});

export default MaquinaScreen;
